function inventoryVIewModel(){
    var self = this;

    var iconTypes = [
        {icon: "icon-bone", text: "Bone"},
        {icon: "icon-ball", text: "Ball"},
        {icon: "icon-circle", text: "Circle"},
        {icon: "icon-rabbit", text: "rabbit"},
    ]

    self.inventory = ko.observableArray([
    ]);
    self.addItem = function() {
        var index = Math.floor(Math.random() * iconTypes.length);
        self.inventory.push(iconTypes[index])
        console.log(index)
    }
    self.removeItem = function(data, event) {
       var indexToRemove = event.target.getAttribute("item-index");
       self.inventory.splice(indexToRemove, 1)
       console.log(indexToRemove)
    }
};

const knockoutApp = document.querySelector("#knockout-app");
ko.applyBindings(new inventoryVIewModel(), knockoutApp);